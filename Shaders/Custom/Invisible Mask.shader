﻿Shader "Universal Render Pipeline/Custom/Invisible Mask"
{
    SubShader 
    {
        Tags 
        {
            "Queue"="AlphaTest+51"
            "RenderPipeline" = "UniversalPipeline"
            "IgnoreProjector" = "True"
        }

        Pass 
        {
            // Keep the image behind it
            Blend Zero One 
        }
    } 
}